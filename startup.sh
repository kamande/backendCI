#!/usr/bin/env bash
# Author Gidraff

set -eux

start_up(){
    apt-get update
    pip install -r requirements.txt
    if [ -d "migrations" ]; then
        rm -rf migrations
    fi
}

setup_db(){
    /etc/init.d/postgresql start
    psql -U postgres -p 5432 -c "ALTER USER postgres with password 'postgres';"
    service postgresql restart
    export APP_SETTINGS="production"
    export DATABASE_URL="postgresql://postgres:postgres@localhost:5432/flask_db"
}


run_migrations(){
    python manage.py db init
    python manage.py db migrate
    python manage.py db upgrade
    python manage.py runserver
}

main(){
    start_up
    setup_db
    run_migrations
}

main"$@"