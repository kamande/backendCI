FROM gidraff/cp-image:v0.0.3

USER root

RUN apt-get update 

RUN service postgresql start

RUN mkdir project

ADD .  project/
WORKDIR  project
RUN rm -rf migrations
RUN chmod 777 startup.sh


USER postgres

RUN /etc/init.d/postgresql start &&\
    psql -U postgres -p 5432 -c "ALTER USER postgres with password 'postgres';" &&\
    psql -U postgres -p 5432 -c "CREATE DATABASE flask_db  OWNER postgres;"

RUN  exit
USER root
RUN service postgresql restart
RUN export APP_SETTINGS="production" &&\
    export DATABASE_URL="postgresql://postgres:postgres@localhost:5432/flask_db"

RUN  service postgresql restart

EXPOSE 5432
ENTRYPOINT ./startup.sh







