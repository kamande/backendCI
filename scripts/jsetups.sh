#!/usr/bin/env bash
# Author Gidraff

set -eux


repo_setup(){

    # wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
    # sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
    # sudo apt-get update
    # sudo apt-get install jenkins

    # add key to verify package repository
    wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -

    # update the source list with the jenkins repository
    echo deb https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list
}

set_firewall(){
    echo "configuring firewall..."
    sudo ufw allow 8080
    sudo ufw allow ssh
    sudo ufw allow 22
    echo "y" | sudo ufw enable
}

min_configs(){
    # pushd /var/lib/apt
    # sudo rm -rf lists.old
    # sudo mv lists lists.cleaold
    # sudo mkdir -p lists/partial
    # sudo apt-get update
    # popd
}

setup_jenkins(){
    echo "Setting up jenkins..."
    sudo apt upgrade -y
    sudo apt install software-properties-common -y
    sudo add-apt-repository ppa:openjdk-r/ppa -y
    sudo apt update
    sudo apt install openjdk-8-jdk  -y
    sudo apt install jenkins -y
}

startup_jenkins(){
    echo "Starting Jenkins..."
    sudo service jenkins start
    echo $(sudo service jenkins status)
}

main(){
    echo "Starting script..."
    repo_setup
    set_firewall
    min_configs
    setup_jenkins
    startup_jenkins
    sudo cat /var/lib/jenkins/secrets/initialAdminPassword
    echo "Ending script..."
}

main"$@"